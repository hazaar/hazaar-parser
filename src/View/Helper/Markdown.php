<?php
/**
 * @file        Hazaar/View/Helper/Markdown.php
 *
 * @author      Jamie Carl <jamie@hazaarlabs.com>
 *
 * @copyright   Copyright (c) 2018 Jamie Carl (http://www.hazaarlabs.com)
 */

namespace Hazaar\View\Helper;

/**
 * @brief       Helper to convert Markdown markup into HTML
 *
 * @detail      The Markdown helper uses the erusev/parsedown text parser to parse text formatted in Markdown syntax.
 *
 *              For details on how to format text using Markdown see [[Using Markdown]]
 *
 *              To use the Markdown helper, add the helper to your view from your [[Hazaar\Controller\Action|Action Controller]]:
 *
 *              pre. $this->view->addHelper('mardown');
 *
 *              Then from within your view you can call the markdown parser:
 *
 *              pre. <?=$this->markdown->parse($this->mymarkdowncontent);?>
 *
 * @since       2.3.59
 */
class Markdown extends \Hazaar\View\Helper {

    private $parser;

    public function import(){

        if(!class_exists('\cebe\markdown\MarkdownExtra'))
            throw new \Exception('cebe\markdown module not available.  Please update Composer to install required dependencies.');

    }

    public function init($view, $args = array()) {

        $this->parser = new \Hazaar\Parser\Markdown(ake($args, 'extensions'));

    }

    public function parse($string) {

        return $this->parser->parse($string);

    }

}

