<?php
/**
 * @file        Hazaar/View/Helper/Geshi.php
 *
 * @author      Jamie Carl <jamie@hazaarlabs.com>
 *
 * @copyright   Copyright (c) 2018 Jamie Carl (http://www.hazaarlabs.com)
 */

namespace Hazaar\View\Helper;

/**
 * @brief       Helper to convert code into syntactically highlighted code.
 *
 * @detail      The GeSHI helper uses the geshi\geshi text parser by to parse source code into pretty HTML.
 *
 *              For details on how to format text using GeSHI see [[Using GeSHI]]
 *
 *              To use the GeSHI helper, add the helper to your view from your [[Hazaar\Controller\Action|Action Controller]]:
 *
 *              pre. $this->view->addHelper('geshi');
 *
 *              Then from within your view you can call the GeSHI parser:
 *
 *              pre. <?=$this->geshi->parse($this->mysourcecode);?>
 *
 * @since       2.3.59
 */
class Geshi extends \Hazaar\View\Helper {

    private $parser;

    public function import(){

        if(!class_exists('GeSHi'))
            throw new \Exception('erusev/parsedown module not available.  Please update Composer to install required dependencies.');

    }

    public function init($view, $args = array()) {

        $this->parser = new \GeSHi();

    }

    public function parse($string, $lang = 'php'){

        $this->parser->set_source($string);

        $this->parser->set_language($lang);

        return $this->parser->parse_code();

    }

}

