<?php

/**
 * @package     Parser
 */
namespace Hazaar\Parser;

class YAML {

    public function __construct() {

    }

    public function parseFile($filename){

        if(!\file_exists($filename))
            return false;

        return $this->parse(file_get_contents($filename));

    }

    public function parse($content){

        $yaml = array();

        $iter = explode("\n", $content);

        reset($iter);

        $this->parseSection($iter, $yaml);

        return $yaml;

    }

    private function parseSection(&$iter, &$parent, $level = 0, &$indents = array()){

        do{

            $item = current($iter);
        
            if(($pos = strpos($item, ':')) === false){

                if(strpos($item, '-') === false)
                    throw new \Exception('Invalid YAML!');

                $listKey = key($parent);

                if(!is_array($parent[$listKey]))
                    $parent[$listKey] = array();

                if(preg_match('/(\s*)\-\s+(\w+)/', $item, $matches))
                    $parent[$listKey][] = $matches[2];

            }else{

                $key = substr($item, 0, $pos);

                $value = trim(substr($item, $pos + 1));

                if(($len = strpos($key, '-')) !== false){

                    $key = trim(substr($key, $len + 1));

                    $listKey = key($parent);

                    if(\array_key_exists($level, $indents) && $len === $indents[$level]){

                        $parent[$key] = $value;

                        end($parent);

                    }elseif(\array_key_exists($level, $indents) && $len < $indents[$level]){

                        prev($iter);

                        return;

                    }else{

                        if(!is_array($parent[$listKey]))
                            $parent[$listKey] = array();

                        $indents[++$level] = $len;

                        $this->parseSection($iter, $parent[$listKey], $level, $indents);

                        unset($indents[$level--]);

                    }

                }elseif($level > 0){

                    prev($iter);

                    return;

                }else{

                    $parent[$key] = $value;

                    end($parent);

                }

            }

        }while(next($iter));

    }

}
