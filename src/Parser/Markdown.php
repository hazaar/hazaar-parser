<?php

namespace Hazaar\Parser;

/**
 * Markdown short summary.
 *
 * Markdown description.
 *
 * @version 1.0
 * @author JamieCarl
 */
class Markdown extends \cebe\markdown\GithubMarkdown {

    private $extensions;

    function __construct($extensions = null){

        if($extensions !== null)
            $this->extensions = $extensions;

    }

	protected function identifyAdmonition($line, $lines, $current) {

        if($this->extensions && in_array('admonition', $this->extensions)
            && substr($lines[$current], 0, 3) === '!!!')
            return true;

        return false;

	}

    protected function consumeAdmonition($lines, $current){

        $block = array(
            'admonition',
            'type' => null,
            'content' => array()
        );

        $header = $lines[$current++];

        if(preg_match('/\!{3}\s(\w+)(\s\"([\w|\s]*)\")?/', $header, $matches)){

            $block['type'] = strtolower($matches[1]);

            $block['title'] = array_key_exists(2, $matches) ? $matches[3] : ucfirst($block['type']);

        }

        for($i = $current, $count = count($lines); $i < $count; $i++){

            //Find the first empty line or non-indented line.
            if(count($block['content']) > 0 && ($lines[$i] === '' || $lines[$i][0] !== ' '))
                break;

            $block['content'][] = trim($lines[$i]);

        }

		return [$block, $i++];

	}

    protected function renderAdmonition($block){

        $out = '<p class="admonition-body">' . implode("\n", $block['content']) . '</p>';

        if($block['type'])
            $out = '<div class="admonition ' . $block['type'] . '">'
                . (($block['title'] !== '') ? '<p class="admonition-title">' . ucfirst($block['title']) . '</p>' : '')
                . $out . '</div>';

        return $out;

    }

}
