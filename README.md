## Hazaar Parser

Text parser plugin for Hazaar MVC

[Documentation](http://hazaarmvc.com/docs)

### Installation
#### Composer
Install the [composer package] by running the following command:

    composer require hazaarlabs/hazaar-parser

### Example

#### Controller

```php
class MyController extends \Hazaar\Controller\Action {

    public function index(){
    
        $this->view('test');
        
        $this->view->addHelper('markdown');
        
    }
    
}
```

#### View
```php
<div>
    <?=$this->markdown->parse("# Hello, World\n\nThis is a test.\n");
</div>
```
